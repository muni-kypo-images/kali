# Changelog

## [qemu-0.4.0] - 2024-12-05
### Changed
- Kali version 2024.3

## [qemu-0.3.1] - 2024-02-01
### Fixed
- Authentication is required to create a color managed device popup after login

## [qemu-0.3.0], [vbox-0.3.0] - 2024-01-30
### Changed
- Kali version 2023.4
- VBox: GuestAdditions 6.1.48

## [qemu-0.2.0], [vbox-0.2.0] - 2023-06-09
### Added
- A build for Vagrant
### Changed
- Unified versioning for QEMU and Vagrant

## [qemu-0.1.2] - 2023-04-19
### Changed
- Update Kali version to 2023.1
### Fixed
- Screen no longer locks after inactivity

## [qemu-0.1.1], [vbox-0.1.1] - 2022-05-16
### Changed
- Update kali version

## [qemu-0.1.0], [vbox-0.1.0] - 2022-02-14
### Added
- VNC server for guacamole access
### Fixed
- Hostname is added on boot to /etc/hosts for qemu

[qemu-0.1.0]: https://gitlab.ics.muni.cz/muni-kypo-images/kali/-/tree/qemu-0.1.0
[vbox-0.1.0]: https://gitlab.ics.muni.cz/muni-kypo-images/kali/-/tree/vbox-0.1.0
[qemu-0.1.1]: https://gitlab.ics.muni.cz/muni-kypo-images/kali/-/tree/qemu-0.1.1
[vbox-0.1.1]: https://gitlab.ics.muni.cz/muni-kypo-images/kali/-/tree/vbox-0.1.1
[qemu-0.1.2]: https://gitlab.ics.muni.cz/muni-kypo-images/kali/-/tree/qemu-0.1.2
[vbox-0.2.0]: https://gitlab.ics.muni.cz/muni-kypo-images/kali/-/tree/vbox-0.2.0
[qemu-0.2.0]: https://gitlab.ics.muni.cz/muni-kypo-images/kali/-/tree/qemu-0.2.0
[vbox-0.3.0]: https://gitlab.ics.muni.cz/muni-kypo-images/kali/-/tree/vbox-0.3.0
[qemu-0.3.0]: https://gitlab.ics.muni.cz/muni-kypo-images/kali/-/tree/qemu-0.3.0
[qemu-0.3.1]: https://gitlab.ics.muni.cz/muni-kypo-images/kali/-/tree/qemu-0.3.1
[qemu-0.4.0]: https://gitlab.ics.muni.cz/muni-kypo-images/kali/-/tree/qemu-0.4.0
